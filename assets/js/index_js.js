$w = $(window).width();
$h = $(window).height();

$(document).ready(
	function(){
		$(".container").css("width",$w);
		$(".container").css("height",$h);
		
		$(".row_2").css("height",$h - 45);
		$(".row_2_1").css("height","80%");
		$(".row_2_2").css("height","20%");
		$(".row_2_1_1").css("height","100%");
		$(".row_2_1_1").css("width","70%");
		$(".row_2_1_2").css("height","100%");
		$(".row_2_1_2").css("width","20%");
		
		$(".row_2_2_1").css("height","100%");
		$(".row_2_2_1").css("width","70%");
		$(".row_2_2_2").css("height","100%");
		$(".row_2_2_2").css("width","20%");
		
		$(".flow object").css("width","100%");
		$(".flow object").css("height","100%");

	}
);

$(document).ready(
	function(){
		$(".send_but").click(
			function(){
				$msg = $(".textbox input").val();
				$(".text_area").append('<div class="chat_spiel"><span class="to_chat flex_centered_right">'+$msg+'</span></div>');
			}	
		);
		
		$(".textbox input").keyup(
			function(e){
				if(e.keyCode == 13){
					$msg = $(".textbox input").val();
					$(".text_area").append('<div class="chat_spiel"><span class="to_chat flex_centered_right">'+$msg+'</span></div>');
				}
			}	
		);
		
		$(".arrow_container").click(
			function(){
				if($(".row_2_1_1").css("width") > "200px"){
					
					$(".row_2_1_1").css("width","87%");
					$(".row_2_1_2").css("width","3%");
					$(".info").css("display","none");
					$(".options").css("display","none");
					$(".textbox").css("display","none");
					$(".text_area").css("display","none");
					$(".send").css("display","none");
					$(".arrow_container").css("position","relative");
					$(".arrow_container").css("left","0%");
					$(".arrow_container").addClass("fliphori");
				}
				else{
					$(".row_2_1_1").css("width","70%");
					$(".row_2_1_2").css("width","20%");
					$(".info").css("display","block");
					$(".options").css("display","block");
					$(".textbox").css("display","block");
					$(".text_area").css("display","block");
					$(".send").css("display","block");
					$(".arrow_container").css("position","absolute");
					$(".arrow_container").css("left","90%");
					$(".arrow_container").removeClass("fliphori");
				}
			}
		);
	}
);