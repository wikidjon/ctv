<!doctype html>
<html>

<head>

	<title>
		Campus TV
	</title>
	
	<link rel="stylesheet" href="assets/css/css/foundation.css" />
	<link rel="stylesheet" href="assets/css/css/normalize.css" />
	<link rel="stylesheet" href="assets/css/index_style.css" />
	<link rel="stylesheet" href="assets/css/fonts.css" />
	<link rel="fav icon" href="assets/img/logo.png" type="image/icon" />
	
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/index_js.js"></script>
	
	<script type="text/javascript" src="assets/js/flowplayer-3.2.13.min.js"></script>
  <!-- Piwik -->
<script type="text/javascript">
flowplayer(function (api) {
   api.bind("load", function (e, api, video) {
      $("#videosrc").text(video.src);
   });
});
</script>
<!-- End Piwik Code -->

</head>

<body>
	<section class="container">
	
	<header class="row_1 flex_centered">
		<section class="column medium-12" style="padding:8px 0px 10px 75px;">
			<span><img src="assets/img/logo.png" class="logo"></span>
		</section>
	</header>
	
	<section class="row_2">
		<section class="row_2_1 apply_brdr flex_centered ">
			<section class="column row_2_1_1 flex_centered_right apply_brdr padding_15">
				<article class="flow flex_centered">
				
				<div class="flowplayer"
   data-engine="flash"
   data-rtmp="rtmp://streams.karunya.edu/live"
   data-ratio="0.4167">
 
   <video autoplay >
<source type="video/webm" src="http://stream.flowplayer.org/bauhaus.webm">
<source type="video/mp4"  src="http://stream.flowplayer.org/bauhaus.mp4">
<source type="video/flash" src="mp4:bauhaus">
   </video>
 
</div>
 
<p class="info">Playing <span id="videosrc">&nbsp;</span></p>

				
					
				</article>
			</section>
			<section class="column row_2_1_2 apply_brdr padding_15">
				<article class="chat box_shadow">
					<article class="chat_header flex_centered">
						<span class="info" style="color:white;">ChitChat</span> 
						<span class="arrow_container flex_centered"><img src="assets/img/arrow.png" class="arrow_right"></span>
					</article>
					
					<div class="options">
						
					</div>
					<div class="text_area">
						<div class="chat_spiel"><span class="from_chat flex_left">
							Hey
						</span></div>
						
						<div class="chat_spiel"><span class="to_chat flex_centered_right">
							Yo!
						</span></div>
						
						<div class="chat_spiel"><span class="from_chat flex_left">
							Hey I am Jonathan, Nice to meet you Buddy. I am pursung my B.tech in Karunya University
						</span></div>
						
						<div class="chat_spiel"><span class="to_chat flex_centered_right">
							Yo!
						</span></div>
					</div>
					
					<div class="textbox">
						<input type="text" placeholder="Type your message here hero!">
					</div>
					
					<div class="send flex_centered">
						<input type="button" class="send_but" style="color:white;" value="Send">
					</div>
					
				</article>
			</section>
		</section>
		
		<section class="row_2_2 flex_centered apply_brdr">
			<section class="column row_2_2_1 apply_brdr padding_15">
				<article class="video_info flex-top_bottom box_shadow">
					<div>
						<span class="info">
							Video Info
						</span>
					</div>
					<div>	
						<span class="info_small">
							Hey there this is KU Campus TV and we telecast random videos over our Intranet!
						</span>
					</div>
				</article>
			</section>
			<section class="column row_2_2_2 apply_brdr padding_15">
				<article class="views flex_centered_bottom">
					<span class="online">377 Viewers Online</span>
				</article>
			</section>
		</section>
	</section>
	
	</section>
	<script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>